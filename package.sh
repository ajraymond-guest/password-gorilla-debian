#!/bin/bash

# Usage: ./package.sh [debuild options]

# Script defaults to local packaging only.
# Generated files are put in tmpbuild/
#
# Use MODE=pbuilder to generate source files for upload.
# Generated files are put in ~/pbuilder/sid_result/

# General settings
PROJ_NAME="password-gorilla"
PROJ_VERSION="1.6.0~git20171121.9209766"
PROJ_DEBREV="1"
PROJ_COMMIT="92097661b919d1c1cc0566e4e2044f974c9ba40f"
PROJ_BASE_URL="https://github.com/zdia/gorilla"


# Environment variables supported by the script
MODE="${MODE:=host}"
([ "${MODE}" = pbuilder ] || [ "${MODE}" = host ]) || {
    echo "ERROR: Invalid MODE parameter (host/builder)"
    exit 1
}


PROJ_URL="${PROJ_BASE_URL}/archive/${PROJ_COMMIT}.tar.gz"
PROJ_URL_FILE="${PROJ_NAME}_`basename \"$PROJ_URL\"`"
PROJ_URL_FILE_BASE=`echo "$PROJ_URL_FILE" | cut -d. -f1`
echo "Project file is: $PROJ_URL_FILE ($PROJ_URL_FILE_BASE)"

rm -f "${PROJ_URL_FILE:-null}"
rm -rf tmpbuild/
rm -rf "${PROJ_URL_FILE_BASE:-null}"

wget "$PROJ_URL" -O "$PROJ_URL_FILE" || {
    echo "ERROR: Could not download selected upstream version"
    exit 1
}

mkdir -p "tmpbuild/${PROJ_NAME}-${PROJ_VERSION}" || {
    echo "ERROR: could not create tmpbuild directory"
    exit 1
}

# password-gorilla specific actions:
#   remove compiled libraries from original package
mkdir "$PROJ_URL_FILE_BASE" || {
    echo "ERROR: Unable to create directory"
    exit 1
}

tar xvf "$PROJ_URL_FILE" --exclude '*.so' --exclude '*.dylib' --exclude '*.dll' --strip-components=1 --directory "$PROJ_URL_FILE_BASE" || {
    echo "ERROR: Could not extract original tar.gz"
    exit 1
}

tar cvzf "tmpbuild/${PROJ_NAME}_${PROJ_VERSION}.orig.tar.gz" "$PROJ_URL_FILE_BASE" || {
    echo "ERROR: Could not copy tarball archive to build directory"
    exit 1
}

rm -rf "${PROJ_URL_FILE_BASE:-null}" || {
    echo "ERROR: Unable to remove temporary directory"
    exit 1
}
# END password-gorilla specific actions

cd tmpbuild/

tar xvz --strip-components=1 -f "${PROJ_NAME}_${PROJ_VERSION}.orig.tar.gz" --directory "${PROJ_NAME}-${PROJ_VERSION}" || {
    echo "ERROR: Could not extract archive for build"
    exit 1
}

cp -r ../debian ${PROJ_NAME}-${PROJ_VERSION}/ || {
    echo "ERROR: Could not copy debian directory for build"
    exit 1
}

cd "${PROJ_NAME}-${PROJ_VERSION}" || {
    echo "ERROR: Unable to enter project source directory"
    exit 1
}

if [ "${SIGN}" = n ]; then
    SIGNOPT=" -us -uc "
else
    SIGNOPT=""
fi

# When doing a pbuilder run, don't need to build binary packages with debuild
if [ "${MODE}" = pbuilder ]; then
    BUILDTYPEOPT=" -S "
else
    BUILDTYPEOPT=""
fi

#lintian complains because of some confusion between ubuntu/debian
#debuild ${BUILDTYPEOPT} ${SIGNOPT} "$@" --lintian-opts --pedantic -i -I --show-overrides || {
debuild ${BUILDTYPEOPT} ${SIGNOPT} "$@" --lintian-opts -i -I --show-overrides || {
    echo "ERROR: debuild failed to create source package"
    exit 1
}

CPU_ARCH=`uname -m`
if [ "${MODE}" = pbuilder ]; then
    if [ "${CPU_ARCH}" = "x86_64" ]; then
        pbuilder-dist sid amd64 clean || {
            echo "ERROR: pbuilder-dist clean"
            exit 1
        }
        pbuilder-dist sid amd64 update || {
            echo "ERROR: pbuilder-dist update"
            exit 1
        }
        pbuilder-dist sid amd64 build ../${PROJ_NAME}_${PROJ_VERSION}-*.dsc || {
            echo "ERROR: pbuilder-dist build"
            exit 1
        }
    else
        pbuilder-dist sid i386 clean || {
            echo "ERROR: pbuilder-dist clean"
            exit 1
        }
        pbuilder-dist sid i386 update || {
            echo "ERROR: pbuilder-dist update"
            exit 1
        }
        pbuilder-dist sid i386 build ../${PROJ_NAME}_${PROJ_VERSION}-*.dsc || {
            echo "ERROR: pbuilder-dist build"
            exit 1
        }
    fi
fi

